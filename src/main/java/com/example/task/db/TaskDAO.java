package com.example.task.db;

import com.example.task.core.Task;
import org.jdbi.v3.sqlobject.config.RegisterBeanMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;

import java.sql.Timestamp;
import java.util.List;

public interface TaskDAO {

    @SqlQuery("select * from task where id = :id")
    @RegisterBeanMapper(Task.class)
    Task findById(@Bind("id") long id);

    @SqlQuery("select * from task")
    @RegisterBeanMapper(Task.class)
    List<Task> findALL();

    @SqlUpdate("insert into task (content,created_at,status,updated_at) values (:content,:created_at,1,:updated_at)")
    void created_task(@Bind("content") String content, @Bind("created_at") Timestamp created_at,@Bind("updated_at") Timestamp update_at);

    @SqlUpdate("UPDATE task SET updated_at=:updated_at,status=:status WHERE id=:id")
    void update_task_status(@Bind("id") long id,@Bind("updated_at") Timestamp updated_at,@Bind("status") String status);

    @SqlUpdate("DELETE FROM task WHERE id=:id")
    void delete(@Bind("id") long id);
}