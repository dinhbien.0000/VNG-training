package com.example.task;

import com.example.task.db.TaskDAO;
import com.example.task.resources.TaskResource;
import io.dropwizard.Application;
import io.dropwizard.db.DataSourceFactory;
import io.dropwizard.jdbi3.JdbiFactory;
import io.dropwizard.migrations.MigrationsBundle;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import org.jdbi.v3.core.Jdbi;

public class TaskApplication extends Application<TaskConfiguration> {
    public static void main(String[] args) throws Exception {
        new TaskApplication().run(args);
    }
    @Override
    public String getName() {
        return "hello-world";
    }

    @Override
    public void initialize(Bootstrap<TaskConfiguration> bootstrap) {
        bootstrap.addBundle(new MigrationsBundle<TaskConfiguration> (){

            @Override
            public DataSourceFactory getDataSourceFactory(TaskConfiguration configuration) {
                return configuration.getDataSourceFactory();
            }
        });
    }

    @Override
    public void run(final TaskConfiguration config,
                   final Environment environment) {
        final JdbiFactory factory = new JdbiFactory();
        final Jdbi jdbi = factory.build(environment, config.getDataSourceFactory(), "mysql");
        final TaskDAO dao = jdbi.onDemand(TaskDAO.class);
        environment.jersey().register(new TaskResource(dao));
    }
}
