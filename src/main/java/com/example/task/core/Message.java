package com.example.task.core;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Message {


    public String message;

    public Message(String message) {
        this.message=message;
    }
    @JsonProperty
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
