package com.example.task.core;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.jdbi.v3.core.mapper.reflect.ColumnName;

import java.sql.Timestamp;

public class Task {

    public Task() {

    }

    public int id;
    public String content;
    public String status;
    public Timestamp created_at;
    public Timestamp updated_at;

    @ColumnName("id")
    @JsonProperty
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
    @ColumnName("content")
    @JsonProperty
    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
    @ColumnName("status")
    @JsonProperty
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
    @ColumnName("created_at")
    @JsonProperty
    public Timestamp getCreated_at() {
        return created_at;
    }

    public void setCreated_at(Timestamp created_at) {
        this.created_at = created_at;
    }

    @ColumnName("updated_at")
    @JsonProperty
    public Timestamp getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(Timestamp update_at) {
        this.updated_at = update_at;
    }
}
