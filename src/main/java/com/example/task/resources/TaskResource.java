package com.example.task.resources;


import com.codahale.metrics.annotation.Timed;
import com.example.task.core.Message;
import com.example.task.core.Task;
import com.example.task.db.TaskDAO;
import io.dropwizard.jersey.params.LongParam;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.sql.Timestamp;
import java.util.List;

@Path("/")
@Produces(MediaType.APPLICATION_JSON)

public class TaskResource {
    private final TaskDAO taskDAO;

    public TaskResource(TaskDAO taskDAO) {
        this.taskDAO = taskDAO;
    }

    @GET
    @Path("/task/{id}")
    public Task findTask(@PathParam("id") LongParam id) {
        return taskDAO.findById(id.get());
    }

    @GET
    @Timed
    @Path("/tasks")
    public List<Task> findALL() {
        return taskDAO.findALL();
    }
    @POST
    @Timed
    @Path("/task")
    public Message add(@QueryParam("content") String content) {
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        taskDAO.created_task(content,timestamp,timestamp);
        return new Message("Created Succses");
    }

    @PUT
    @Timed
    @Path("/task/{id}/{status}")
    public Message Task_Updated(@PathParam("id") Integer id, @PathParam("status") String status) {
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        taskDAO.update_task_status(id,timestamp,status);
        return new Message("Update Success");
    }

    @DELETE
    @Timed
    @Path("/task/{id}")
    public Message delete(@PathParam("id") LongParam id) {
        taskDAO.delete(id.get());
        return new Message("Delete Success");
    }

}
